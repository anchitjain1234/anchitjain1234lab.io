---
layout: page
title: About
permalink: /about/
---
Hi there,

I am Anchit Jain. I graduated from [Birla Institute of Technology and Science Pilani][bits] (BITS PILANI), Pilani Campus in 2017 majoring in Computer Science and Economics. I am currently working as a  Software Engineer in Samsung R&D Institute Bangalore.

You could check my résumé [here][resume].

Need to contact me? Mail me at anchitjain1234 at gmail dot com  
or find me on: 
<ul>
<li><a href="https://github.com/anchitjain1234">Github</a></li>
<li><a href="https://linkedin.com/in/anchitjain1234">LinkedIn</a></li>
<li><a href="https://twitter.com/anchitjain1234">Twitter</a></li>
<li><a href="https://www.facebook.com/anchit.jain.1234">Facebook</a></li>
</ul>
Sensitive communication? View my public key at [Keybase][keybase].

Thanks for visiting my site.

[bits]: http://www.bits-pilani.ac.in/
[resume]: ../assets/resume.pdf/
[keybase]: https://keybase.io/anchitjain1234/
